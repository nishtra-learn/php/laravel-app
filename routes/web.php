<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('test/index', 'TestController@index');
Route::get('test/show', 'TestController@show');
Route::resource('topic', 'TopicController');
Route::resource('block', 'BlockController');

// Home
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')
    ->name('home');
Route::get('home/{topicid}', 'HomeController@filterByTopic')
    ->where('topicid', '[1-9]+');
Route::get('home/search', 'HomeController@filterTopics');
Route::get('home/register', 'HomeController@register');
Route::post('home/showuser', 'HomeController@showuser');

Auth::routes();
