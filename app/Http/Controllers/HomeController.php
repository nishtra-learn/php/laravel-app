<?php

namespace App\Http\Controllers;

use App\Models\Block;
use App\Models\Topic;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index() {
        $topics = Topic::all();
        $page = "main";
        $id = 0;
        $blocks = Block::all();
        return view('home.index', ["topics" => $topics, "page" => $page, "id" => $id, "blocks" => $blocks]);
    }

    public function filterByTopic($topicid)
    {
        $topics = Topic::all();
        $blocks = Block::where("topicid", "=", $topicid)->get();
        $page = "main";
        return view("home.index", ["topics" => $topics, "blocks" => $blocks, "page" => $page, "id" => $topicid]);
    }

    public function filterTopics(Request $request) {
        $searchKey = $request->search;
        $topics = null;
        if ($searchKey) {
            $filter = "%".$searchKey."%";
            $topics = Topic::where("topicname", "like", $filter)->get();
        }
        else {
            $topics = Topic::all();
        }
        $topicIds = $topics->modelKeys();
        $page = "main";
        $id = 0;
        $blocks = Block::whereIn("topicid", $topicIds)->get();
        return view('home.index', ["topics" => $topics, "page" => $page, "id" => $id, "blocks" => $blocks, "search" => $searchKey]);
    }

    /**
     * REGISTER USER
     * */
    public function register() {
        return view('home.register', ["page" => ""]);
    }

    public function showuser(Request $request) {
        $errors = null;
        $warning = null;
        $user = null;

        if (!$request->login) {
            $errors[] = "Missing login";
        }
        if (!$request->email) {
            $errors[] = "Missing email";
        }
        if (!$request->age) {
            $errors[] = "Missing age";
        }
        if (!$request->pass) {
            $errors[] = "Missing password";
        }
        if ($request->pass !== $request->passConf) {
            $errors[] = "Password confirmation doesn't match";
        }

        if (!$errors && $request->age < 18) {
            $warning = "Content 18+. Access denied.";
        }

        // this is the only place that works with user so use array instead of creating a model
        $user["login"] = $request->login;
        $user["email"] = $request->email;
        $user["age"] = $request->age;

        return view('home.showuser', ["page" => "", "errors" => $errors, "warning" => $warning, "user" => $user]);
    }
}
