<?php

namespace App\Http\Controllers;

use App\Models\Block;
use App\Models\Topic;
use Illuminate\Http\Request;
use Route;

class TopicController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $topics = Topic::all();
        // $page = "main";
        // $id = 0;
        // $blocks = Block::all();
        // return \view('topic.index', ["topics" => $topics, "page" => $page, "id" => $id, "blocks" => $blocks]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $topic = new Topic();
        $page = "topic.create";
        return view("topic.create", ["page" => $page, "topic" => $topic]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            "topicname" => "required|unique:topics,topicname|max:100",
        ]);

        $topic = new Topic();
        $topic->topicname = $request->topicname;

        if (!$topic->save()) {
            $err = $topic->getError();
            return redirect()
                ->action("TopicController@create")
                ->withErrors("errors", $err)
                ->withInput();
        }
        else {
            $message = "Topic created";
            return redirect()->action("TopicController@create")->with("message", $message);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $topics = Topic::all();
        // $blocks = Block::where("topicid", "=", $id)->get();
        // $page = "main";
        // return view("topic.index", ["topics" => $topics, "blocks" => $blocks, "page" => $page, "id" => $id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
