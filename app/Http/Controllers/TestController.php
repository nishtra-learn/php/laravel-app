<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TestController extends Controller
{
    public function index() {
        return view("test.index", ["page" => "Index"]);
    }

    public function show() {
        $r = rand(0, 1000);
        $title = ".blade expample";
        $countries = ["Ukraine", "Italy", "Russia", "Japan"];
        return view("test.countries", array("page" => "Countries", "rand" => $r, "title" => $title, "countries" => $countries ));
    }
}
