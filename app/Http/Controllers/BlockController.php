<?php

namespace App\Http\Controllers;

use App\Models\Block;
use App\Models\Topic;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class BlockController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $topics = Topic::all()->pluck("topicname", "id");
        $page = "block.create";
        $block = new Block();
        return view("block.create", ["page" => $page, "topics" => $topics, "block" => $block]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $block = new Block();
        $block->topicid = $request->topicid;
        $block->title = $request->title;
        $block->content = $request->content;

        $file = $request->file("imagepath");
        if ($file) {
            $ogName = $request->file("imagepath")->getClientOriginalName();
            $dirname = "images/";
            $file->move($dirname, $ogName);
            $block->imagepath = $dirname . $ogName;
        }

        $block->save();
        return \redirect("home");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $block = Block::find($id);
        $topics = Topic::all()->pluck("topicname", "id");
        $page = "block.edit";
        return \view("block.edit", ["page" => $page, "topics" => $topics, "block" => $block]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $block = Block::find($id);
        $block->topicid = $request->topicid;
        $block->title = $request->title;
        $block->content = $request->content;

        $updateImage = $request->boolean("changeImage");
        if ($updateImage) {
            $file = $request->file("imagepath");
            if ($file) {
                $ogName = $request->file("imagepath")->getClientOriginalName();
                $dirname = "images/";
                $file->move($dirname, $ogName);
                $block->imagepath = $dirname . $ogName;
            }
            else {
                $block->imagepath = null;
            }
        }

        $block->update();
        return \redirect("home");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $block = Block::find($id);
        $block->delete();
        return \redirect("home");
    }
}
