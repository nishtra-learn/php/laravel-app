@extends('layouts.master')
@section('title', 'Create topic')
@section('content')

{{-- error alert --}}
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $err)
                <li>{{ $err }}</li>
            @endforeach
        </ul>
    </div>
@endif

<h4>
    <span class="badge badge-info w-100">
        Create Topic
    </span>
</h4>
<div>
    {!! Form::model($topic, ["action" => "TopicController@store", "method"=>"POST"]) !!}
    <div class="form-group">
        <div class="row">
            {!! Form::label("topicname", "Topic name", ["class" => "col-md-2 col-form-label"]) !!}
            <div class="col-md-5">
                {{-- {!! Form::text("topicname", "", ["class" => ("form-control col-md-5" . ($errors->has('topicname') ? "is-invalid" : "" ))]) !!} --}}
                <input type="text" name="topicname" id="topicname" value="{{ old('topicname') }}" class="form-control @error('topicname') is-invalid @enderror">
                @error('topicname')
                    <div class="invalid-feedback">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>
    </div>
    {!! Form::submit("Add topic", ["class" => "btn btn-primary"]) !!}
    {!! Form::close() !!}
</div>

@endsection
