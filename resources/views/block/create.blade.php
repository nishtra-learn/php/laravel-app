@extends('layouts.master')
@section('title', 'Create block')
@section('content')

{{-- error alert --}}
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $err)
                <li>{{ $err }}</li>
            @endforeach
        </ul>
    </div>
@endif

<h4>
    <span class="badge badge-info w-100">
        Create Block
    </span>
</h4>

{!! Form::model($block, ["action" => "BlockController@store", "method" => "post", "files" => true]) !!}
<div class="form-group row">
    {!! Form::label("topicid", "Topic", ["class" => "col-md-3"]) !!}
    <div class="col-md-9">
        {!! Form::select("topicid", $topics, "", ["class" => "form-control"]) !!}
    </div>
</div>
<a href="{{ url("topic/create") }}" class="btn btn-primary">Add new topic</a>
<hr>
<div class="form-group row">
    {!! Form::label("title", "Title", ["class" => "col-md-3"]) !!}
    <div class="col-md-9">
        {!! Form::text("title", "", ["class" => "form-control"]) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label("content", "Content", ["class" => "col-md-3"]) !!}
    <div class="col-md-9">
        {!! Form::textarea("content", "", ["class" => "form-control", "rows" => "5"]) !!}
    </div>
</div>
<div class="form-group row">
    {!! Form::label("imagepath", "Image", ["class" => "col-md-3"]) !!}
    <div class="col-md-9">
        {!! Form::file("imagepath", ["class" => "form-control-file", "accept" => "image/*"]) !!}
    </div>
</div>
{!! Form::submit("Add block", ["class" => "btn btn-primary"]) !!}
{!! Form::close() !!}

@endsection
