@extends('layouts.master')
@section('title', 'Register user')
@section('content')

<form action="{{ url('home/showuser') }}" method="post">
    @csrf
    <div class="form-group">
        <label for="login">Login</label>
        <input type="text" name="login" id="login" class="form-control">
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" name="email" id="email" class="form-control">
    </div>
    <div class="form-group">
        <label for="age">Age</label>
        <input type="number" min="0" name="age" id="age" class="form-control">
    </div>
    <div class="form-group">
        <label for="pass">Password</label>
        <input type="password" name="pass" id="pass" class="form-control">
    </div>
    <div class="form-group">
        <label for="passConf">Confirm password</label>
        <input type="password" name="passConf" id="passConf" class="form-control">
    </div>
    <div class="form-group">
        <input type="submit" value="Register" class="btn btn-primary">
    </div>
</form>

@endsection
