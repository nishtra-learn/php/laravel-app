@extends('layouts.master')
@section('content')

@if ($errors)
    <div class="alert alert-danger" role="alert">
        <ul class="mb-0">
            @foreach ($errors as $err)
                <li>{{ $err }}</li>
            @endforeach
        </ul>
    </div>
@elseif ($warning)
    <div class="alert alert-warning" role="alert">
        {{ $warning }}
    </div>
@else
    <div><strong>Login:</strong> {{ $user["login"] }}</div>
    <div><strong>Email:</strong> {{ $user["email"] }}</div>
    <div><strong>Age:</strong> {{ $user["age"] }}</div>
@endif

@endsection
