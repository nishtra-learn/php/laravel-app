{{-- <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <h3>{{ $title }}</h3>
    @if ($rand < 500)
        <div>Rand < 500</div>
    @else @if ($rand === 500)
            <div>Rand = 500</div>
        @else
            <div>Rand > 500</div>
        @endif
    @endif

    <h4>Countries</h4>
    @foreach ($countries as $item)
        <li>{{ $item }}</li>
    @endforeach
</body>

</html> --}}

@extends('layouts.master')
@section('content')

    <h3>{{ $title }}</h3>
    @if ($rand < 500)
        <div>Rand < 500</div>
    @elseif ($rand === 500)
        <div>Rand = 500</div>
    @else
        <div>Rand > 500</div>
    @endif

    <h4>Countries</h4>
    @foreach ($countries as $item)
        <li>{{ $item }}</li>
    @endforeach

@endsection
